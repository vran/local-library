# variables for vagrant

module Variables

# number of virtual maschines include ansible host
  VM_COUNT = 4
# set ethernet card name
  ETHERNET_CARD = "Сетевая карта Realtek RTL8101E Family PCI-E Fast Ethernet NIC (NDIS 6.20)"
#ETHERNET_CARD = "Killer E2400 Gigabit Ethernet Controller"
# number of cpu cores for each virtual maschine
  VM_CPU_CORES = 2
# amount of memory for each virtual machine
  VM_MEMORY = 2048
# network type for project ("private" or "public")
  NETWORK_TYPE = "private"
# ip subnet: first three octets with delimiters
  IP_RANGE = "192.168.100"
# "true" if you need ansible in this project
  NEED_ANSIBLE = false
# type of ansible provision: "ansible" or "ansible_local"
  ANSIBLE_TYPE = "ansible"
# ansible host name
  ANSIBLE_HOSTNAME = "ansible01"
# list of vm names and forwarding ports except ansible. must to be specified as:
# [["vm1-name", "guest-port1:host-port1", .., "guest-portN:host-portN"],
# ..,
# ["vmN-name", "guest-port1:host-port1", .., "guest-portN:host-portN"]]
  VM_HOSTS = [
    ["db01"],
    ["app01","8000:8000"],
    ["web01","80:80"],
    ["mon01","9090:9090"]
  ]

end
