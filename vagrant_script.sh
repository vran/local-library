#!/bin/bash

apt-get update
if [ $(hostname) = "ansible01" ]; then
    apt-get install python3-pip rsync git curl mc docker.io docker-compose -y
    usermod -aG docker vagrant
    mkdir -p /etc/ansible
    cp -r /vagrant/ansible.cfg /etc/ansible/ansible.cfg
    pip3 install ansible
    pip3 install --upgrade ansible-core==2.14
    pip3 install -U git+https://github.com/ansible-community/molecule
    pip3 install requests pytest-testinfra "molecule[docker]" molecule-docker
    pip3 install --upgrade molecule==5.1.0
    cp -r /vagrant/.ssh/id_ed25519* /home/vagrant/.ssh
    chown vagrant:vagrant /home/vagrant/.ssh/id_ed25519
    chown vagrant:vagrant /home/vagrant/.ssh/id_ed25519.pub
    chmod 600 /home/vagrant/.ssh/id_ed25519
    chmod 644 /home/vagrant/.ssh/id_ed25519.pub;
else
    cat /vagrant/.ssh/id_ed25519.pub >> /home/vagrant/.ssh/authorized_keys
    cat /vagrant/.ssh/ansible_key.pub >> /home/vagrant/.ssh/authorized_keys;
fi