FROM python:3.9-slim-bullseye

RUN apt-get update &&\
    apt-get upgrade -y &&\
    apt-get install postgresql-client supervisor -y &&\
    pip3 install --upgrade pip

RUN mkdir /usr/locallibrary
WORKDIR /usr/locallibrary

COPY ./locallibrary/code /usr/locallibrary
COPY ./locallibrary/config/gunicorn/ /usr/locallibrary/config/
RUN pip3 install --no-cache-dir -r requirements.txt &&\
    sed -i "s/ALLOWED_HOSTS = \['.railway.app','127.0.0.1'\]/ALLOWED_HOSTS = \['.railway.app','127.0.0.1','localhost','app'\]/g" /usr/locallibrary/locallibrary/settings.py &&\
    sed -i "s/'NAME': 'mydatabase',/'NAME': 'aisks',/g" /usr/locallibrary/locallibrary/settings.py &&\
    sed -i "s/'HOST': '127.0.0.1',/'HOST': 'db',/g" /usr/locallibrary/locallibrary/settings.py &&\
    python3 /usr/locallibrary/manage.py collectstatic --no-input &&\
    useradd -s /sbin/nologin appuser &&\
    mkdir -p /var/log/supervisor

COPY ./locallibrary/config/supervisor/ /etc/supervisor/

CMD supervisord --nodaemon -c /etc/supervisor/supervisord.conf
