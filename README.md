Local Library: Django project
=============================
Разорачивает приложение Local Library с использованием пайплайнов CI/CD.  
Больше информации о приложении Local Library смотрите тут: <https://gitfront.io/r/deusops/gYR9SeouZXyd/django-locallibrary/>  
Пайплайн CI/CD тестирует код приложения и ansible, собирает docker-образ, затем разворачивает необходимую инфраструктуру и запускает приложение. Для окружения *development* это делается автоматически, для *production* - развертывание и запуск выполняется в ручном режиме.  
Так же в ручном режиме можно запустить сборку и отправку контейнера в репозиторий.  
Для локального тестирования написан docker-compose.  
Запуск протестирован на локльных виртуальных машинах, разворачиваемых с помощью Vagrant. Файлы для запуска ВМ находятся в корне данного репозитория.  
Тут можно посмотреть вариант запуска kubernetes-кластера с этим приложением: <https://gitlab.com/demo-v/locallibrary-in-k8s>  

Требования
----------
* ansible >= 2.14  
* rsync  
* curl  
* git  
* molecule  
* molecule-docker  

В сценарии Ansible используются следующие роли:  
* PostgreSQL: <https://gitlab.com/ansible_roles_v/postgresql.git>  
* Django framework: <https://gitlab.com/ansible_roles_v/django.git>  
* Gunicorn WSGI HTTP Server: <https://gitlab.com/ansible_roles_v/gunicorn.git>  
* Supervisor daemon: <https://gitlab.com/ansible_roles_v/supervisor.git>  
* Nginx: <https://gitlab.com/ansible_roles_v/nginx.git>  
* Prometheus exporter: <https://gitlab.com/ansible_roles_v/node-exporter.git>  
* Prometheus: <https://gitlab.com/ansible_roles_v/prometheus.git>  
Файл requirements.yml для установки необходимых ролей находится в корневой директории проекта. Роли устанавливаются автоматически при запуске pipeline.  
При желании роли можно уставновить вручную:
```bash
ansible-galaxy install -r requirements.yml
```
GitLab runner установлен на отдельный хост с помощью роли <https://gitlab.com/ansible_roles_v/gitlab-runner>  
Gitlab runner должен иметь доступ к мастер-хосту кластера по SSH. У пользователя, под которым выполняется подключение к мастер-хосту, должны быть права на управление кластером.  
Для запуска сценариев используется готовый образ с Ansible: <https://gitlab.com/docker_images_v/ansible/container_registry>  

Переменные в GitLab CI/CD
-------------------------
HOSTS_DEV - ansible hosts-файл для окружения *development*. Образец тут: [group_vars](https://gitlab.com/vran/local-library/-/blob/main/inventories/develop/hosts.example).  
HOSTS_PROD - ansible hosts-файл для окружения *production*. Образец тут: [group_vars](https://gitlab.com/vran/local-library/-/blob/main/inventories/production/hosts.example).  
CREDENTIALS_DEV - файл с секретами для окружения *development*. Образец тут: [group_vars](https://gitlab.com/vran/local-library/-/blob/main/inventories/develop/credentials.yml.example).  
CREDENTIALS_PROD - файл с секретами для окружения *production*. Образец тут: [group_vars](https://gitlab.com/vran/local-library/-/blob/main/inventories/production/credentials.yml.example).  
ANSIBLE_USER - пользователь, под которым ansible подключается к хостам.  
SSH_KEY - ключ для подключения к хостам.  

Так же в самом пайплайне GitLab CI указана переменная *ANSIBLE_INVENTORIES_PATH* - путь к директории с инвентарями.  
Еще одна переменная в пайплайне: *ANSIBLE_PATH* - путь к директории, где лежат playbook.yml и (если нужно) requirements.yml.  
